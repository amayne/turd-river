﻿using UnityEngine;
using System.Collections;
using System;

public class PoopyControlScript : MonoBehaviour {

	public float maxSpeed = 5f;
	public float maxForce = 1f;
	public int   healthPoints = 20;
	bool facingRight = true;

	Animator anim;

	float gravity;
	CharacterMotor motor;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
	}

	void Awake () {
		if(GetComponent<CharacterMotor>())
		{
			motor = GetComponent<CharacterMotor>();
			gravity = motor.movement.gravity;
		} 
	}
	
	// Update is called once per frame
	void FixedUpdate () {
	

		float moveX = Input.GetAxis ("Horizontal");
		float moveY = Input.GetAxis ("Vertical");

		rigidbody2D.AddForce(new Vector2(moveX * maxForce/2*(1+rigidbody2D.velocity.x/maxSpeed) , 
		                                 moveY * maxForce/2*(1+rigidbody2D.velocity.y/maxSpeed)));

		//add drag


			gravity += 1;
			


		if (moveX > 0 && !facingRight) {
			Flip ();
		} else if(moveX < 0 && facingRight) {
			Flip();
		}

		rigidbody2D.transform.rotation = Quaternion.AngleAxis(-15F*rigidbody2D.velocity.x / maxSpeed, new Vector3(0,0,1));
	}

	void Flip () {
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale=theScale;


	}

	void OnCollisionEnter2D (Collision2D coll) {
		//set repel force
		//coll.contacts[0].otherCollider.rigidbody2D.
		anim.SetBool ("Collision", true);
	}
	void OnCollisionExit2D (Collision2D coll) {
		anim.SetBool ("Collision", false);
	}
}
