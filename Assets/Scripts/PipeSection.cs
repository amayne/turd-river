﻿using UnityEngine;
using System.Collections;

public class PipeSection {
	
	//class variables
	public enum FittingType {Straight,Elbow};
	static PipeSection[] pipeForms;
	public static float fittingBuffer = 0.1f;
	public static float pipeWidth = 1.44f;
	public const float FlangeWidth = 0.72f;

	//instance variables
	public GameObject[] walls;
	FittingType type;
	PipeSection next;
	Vector3 orientation;


	// property to get/set the XY position of the outlet for this section
	// value must be float[2] with x at 0th and y at 1st positions
	public Vector3 Outlet {
		get {
			Vector3 outletXY = new Vector3();

			if (this.type == PipeSection.FittingType.Straight) {
				outletXY[0] = (this.walls [1].transform.position.x + this.walls [0].transform.position.x) / 2;
				outletXY[1] = (this.walls [1].transform.position.y + this.walls [0].transform.position.y) / 2;
			} else if (this.type == PipeSection.FittingType.Elbow) {
				outletXY[0] = (this.walls [0].transform.position.x + this.walls [0].renderer.bounds.size.x/2
				               + this.walls [1].transform.position.x + this.walls [1].renderer.bounds.size.x/2
				               - PipeSection.FlangeWidth * this.walls [1].transform.localScale.x) / 2;
				outletXY[1] = (this.walls [0].transform.position.y + this.walls [0].renderer.bounds.size.y/2
				               + this.walls [1].transform.position.y + this.walls [1].renderer.bounds.size.y/2) / 2;
			} 

			return outletXY;
		}

	}

	public Vector3 Inlet {
		get {
			Vector3 inletY = new Vector3();
			inletY = inletY*3;
			inletY[0] = this.walls [0].transform.position.y;
			inletY[1] = this.walls [1].transform.position.y;
			
			return inletY;
		}
		set {

		}
	}

	
	public PipeSection Next {
		get {
			return this.next;
		}
		set {
			this.next = value;
		}
	}

	public bool isVisible {
		get {
			bool temp = true;
			
			foreach (GameObject wall in this.walls) {
				temp &= wall.renderer.isVisible;
			}
			
			return temp;
		}
		
	}

	public Vector3 Orientation {
		get {
				return this.orientation;
		}
		set {

			foreach(GameObject wall in this.walls) {
				wall.transform.Rotate (value);
			}

        }
	}

	public Vector3 OutletOrientation {
		get {
			if (this.type == PipeSection.FittingType.Straight) {
				return this.orientation;
			} else {
				//put other fitting types here
				return this.orientation;

			}
		}
	}
	//----------------------------------Constructors
	
	private PipeSection() {
		//default constructor

		//default pipe has two walls
		walls = new GameObject[2];
	}
	
	public PipeSection(GameObject wall0,GameObject wall1) : this() {
		this.walls [0] = wall0;
		this.walls [1] = wall1;
	}

	private PipeSection Clone() {
		PipeSection slug = new PipeSection ();
		
		slug.walls = new GameObject[this.walls.Length];
		
		for (int i = 0; i < this.walls.Length; i++) {
			
			slug.walls[i] = (GameObject) Object.Instantiate(this.walls[i]);
		}
		
		slug.type = this.type;
		
		return slug;
	}


	//-------------------------------Factory for PipeSection Creation

	private static void StartForge() {
		pipeForms = new PipeSection[1];
		
		pipeForms [(int)FittingType.Straight] = new PipeSection ((GameObject) Resources.Load("top_pipe",typeof(GameObject)),
		                                                         (GameObject) Resources.Load("bottom_pipe",typeof(GameObject)));
	}
	
	public static PipeSection ForgePipeSection(FittingType fitting, PipeSection outletPipe) {
		if (pipeForms == null) {
			// first instance of this class
			// starting forge...
			StartForge();
		}
		
		
		PipeSection slug = pipeForms [(int)fitting].Clone();

		if (outletPipe != null) {
			slug.FitPipe (outletPipe);
		}

		return slug;
	}


	//--------------------------------------Instance Methods
	
	public void Remove() {
		for (int i =0; i < this.walls.Length; i++) {
			Object.Destroy(this.walls[i],0F);
		}
		this.Next = null;
	}
		
	public void FitPipe (PipeSection outletPipe) {
		
		Vector3 movement;

		//orient this pipe to fit correctly with outlet
		this.Orientation = outletPipe.OutletOrientation;

		for (int i = 0; i < this.walls.Length; i++) {
			movement = new Vector3(outletPipe.OutletX[i] - this.InletX[i] + PipeSection.fittingBuffer,
			                   outletPipe.OutletY[i] - this.InletY[i],
			                   0);
			this.walls[i].transform.Translate(movement);

			
		}
		outletPipe.Next = this;
	}

}