﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Plumber : MonoBehaviour {

	public static PipeSection pipeHead; //build pipe at the head
	public static PipeSection pipeTail; //take pipe from the tail
	public static GameObject headApprentice; //the head aprentice warns the plumber to build
	public static GameObject tailApprentice; //the tail apprentice warns the plumber to destroy
	public static float tailApprenticeMargin = 1f;
	public static float headApprenticeMargin = 0f;

	public static void LayPipe () {

		Plumber.pipeHead = PipeSection.ForgePipeSection (PipeSection.FittingType.Straight, pipeHead);
		headApprentice.transform.Translate (new Vector3(headApprenticeMargin + Plumber.pipeHead.OutletX[0] - headApprentice.transform.position.x,0f,0f));
	}

	public static void RemovePipe () {
		if (!Plumber.pipeTail.isVisible || Application.isEditor) {
			PipeSection temp = Plumber.pipeTail.Next;

			Plumber.pipeTail.Remove ();
			Plumber.pipeTail = temp;


			tailApprentice.transform.Translate (new Vector3 (tailApprenticeMargin + Plumber.pipeTail.OutletX [0] - tailApprentice.transform.position.x, 0f, 0f));
		}
	}

	// Use this for initialization
	void Start () {

		
		Plumber.pipeHead = new PipeSection (GameObject.FindGameObjectWithTag("wall0"),
		                           		GameObject.FindGameObjectWithTag("wall1"));
		Plumber.pipeTail = pipeHead;

		headApprentice = GameObject.FindGameObjectWithTag ("HeadApprentice");
		tailApprentice = GameObject.FindGameObjectWithTag ("TailApprentice");

		//Plumber.LayPipe();
	}
	
	// Update is called once per frame
	void Update () {
	}

}